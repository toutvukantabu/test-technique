<?php

namespace App\Tests;


use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\Team;
use App\Entity\User;
use App\Factory\TeamFactory;
use App\Factory\UserFactory;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;


abstract class AbstractTest extends ApiTestCase
{
    private ?string $token = null;

    use ResetDatabase;
    use Factories;

    public function setUp(): void
    {
        self::bootKernel();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function createClientWithCredentials($token = null, User $user = null): Client
    {
        $token = $token ?: $this->getToken();

        return static::createClient([], [
            'base_uri' => 'https://localhost:4443',
            'headers' => ['authorization' => 'Bearer ' . $token]]);
    }

    /**
     * Use other credentials if needed.
     * @param array $body
     * @param User|null $user
     * @return string
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    protected function getToken(array $body = [], User $user = null): string
    {
        if ($this->token) {
            return $this->token;
        }

        if (!$user) {
            $user = $this->createAdmin();
        }
        $response = static::createClient()->request('POST', '/login', ['json' => $body ?: [
            'username' => $user->getEmail(),
            'password' => 'admin',
        ]]);

        $this->assertResponseIsSuccessful();
        $data = $response->toArray();
        $this->token = $data['token'];

        return $data['token'];
    }

    public function createAdmin(): User
    {

        $admin = userFactory::createOne(
            [
                'roles' => ['ROLE_ADMIN'],
            ]
        );
        $admin->assertPersisted();
        return $admin->object();
    }


    public function createTeamLeader(?Team $team = null): User
    {
        if (!$team) {
            $team = $this->createTeamMember(null)->getTeam();
        }

        $userLeader = UserFactory::createOne([
            'team' => $team,
            'roles' => ['ROLE_TEAM_LEADER']
        ]);

        $userLeader->assertPersisted();
        return $userLeader->object();
    }


    public function createTeamMember(?Team $team = null): User{

        if (!$team) {
            $team = TeamFactory::createOne();
        }
        $teamMembers = UserFactory::createOne(
            [
                'team' => $team,
                'roles' => ['ROLE_USER']
            ],
        );
        $teamMembers->assertPersisted();
        return $teamMembers->object();
    }
}
