<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Factory\TeamFactory;
use App\Factory\UserFactory;
use App\Tests\AbstractTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UserTest extends AbstractTest
{

    public User $teamMember;
    public User $teamLeader;
    public User $admin;

    public function setUp(): void
    {

        parent::setUp();
        $this->teamMember =  $this->createTeamMember();
        $this->teamLeader = $this->createTeamLeader($this->teamMember->getTeam());
        $this->admin = $this->createAdmin();
    }


    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testAdminUsers(): void
    {
        $this->createClientWithCredentials(null, $this->admin)->request('GET', '/users');
        $this->assertResponseIsSuccessful();
    }
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testTeamMemberUsers(): void
    {
        UserFactory::createMany(20, [
            'team' => $this->teamMember->getTeam(),
            'roles' => ['ROLE_USER']
        ]);
        $response = $this->createClientWithCredentials(null, $this->teamMember)->request('GET', '/users');
        $this->assertResponseIsSuccessful();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testTeamLeaderUsers( ): void
    {
        $response = $this->createClientWithCredentials(null, $this->teamLeader)->request('GET', '/users');
        $this->assertResponseIsSuccessful();
        $this->assertEquals(200, $response->getStatusCode());
    }
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPostToCreateUser(): void
    {
        $client = $this->createClientWithCredentials(null, $this->teamLeader);
        $response = $client->request('POST', '/users', [
            'json' => [
                'email' => 'testCreateUser@mail.com',
                'firstname' => 'test',
                'lastname' => 'test'

            ]
        ]);
        $this->assertResponseStatusCodeSame(422,"This value is too short. It should have 5 characters or more.",);
        $response = $client->request('POST', '/users', [
            'json' => [
                'email' => 'testCreateUser@mail.com',
                'firstname' => 'testz',
                'lastname' => 'testz'

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertEquals(201, $response->getStatusCode());

        $client->request('POST', '/users', [
            'json' => [
                'email' => 'testCreateUser@mail.com',
                'firstname' => 'testzzz',
                'lastname' => 'testzzz'


            ]
        ]);
        $this->assertResponseStatusCodeSame(422,"This email is already used.",);
    }


    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testDeleteUser(): void
    {

        $user = UserFactory::createOne(
            [
                'team' => $this->teamMember->getTeam(),
                'roles' => ['ROLE_USER']
            ]
        );

        $user->assertPersisted();
        $client = $this->createClientWithCredentials(null, $this->teamLeader);
        $client->request('DELETE', '/users/' . $user->getId());
        $this->assertResponseStatusCodeSame(204);

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testUpdateUser(): void{

        $user = UserFactory::createOne(
            [
                'team' => $this->teamLeader->getTeam(),
                'roles' => ['ROLE_USER']
            ]
        );
        $user->assertPersisted();

        $client = $this->createClientWithCredentials(null, $this->teamLeader);
        $client->request('PATCH', '/users/' . $user->getId(), [
            'json' => [
                'email' => 'testUpdateUser@mail.com',
            ]
        ]);
        $this->assertResponseStatusCodeSame(200);

        $team = TeamFactory::createOne();
        $user2 = UserFactory::createOne(
            [
                'team' => $team,
                'roles' => ['ROLE_USER']
            ]
        );
        $user2->assertPersisted();

        $client = $this->createClientWithCredentials(null, $this->teamLeader);
        $client->request('PATCH', '/users/' . $user2->getId(), [
            'json' => [
                'email' => 'testUpdateUser@mail.com',
            ]
        ]);
        $this->assertResponseStatusCodeSame(422);
    }
}
