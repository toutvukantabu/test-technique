
## Import .env ##
include .env
export

# Executables (local)
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMP = docker compose
LINUX_ROOT= bescont

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php
MARIADB  = symfony-database-1

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
SYMFONY  = $(PHP_CONT) bin/console

# Database
DATABASE_ONE= $(MYSQL_DB)

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.2
PHPQA_RUN = $(DOCKER_RUN) --init --rm -v $(PWD):/project -w /project $(PHPQA)

# Misc
.DEFAULT_GOAL = help
.PHONY        : help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@HTTP_PORT=8000 HTTPS_PORT=4443 HTTP3_PORT=4443 docker compose up -d --wait

start:
	@$(MAKE) build
	@$(MAKE) up
	@echo 'montage des databases veuillez patienter...'
	@$(MAKE) database ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(PHP_CONT) sh

mariadb: ## connecting to MARIADB bash
	@docker exec -it  $(MARIADB) mariadb -u root -pexample $(DATABASE_ONE)

## —— Comp]ser 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor-dev: ## Install vendors according to the current composer.lock file
vendor-dev: c=install --prefer-dist  --no-progress --no-scripts --no-interaction
vendor-dev: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf
cc: clear-doctrine-cache

clear-doctrine-cache:
	@echo "Nettoyage du cache Doctrine..."
	@$(PHP_CONT) bin/console doctrine:cache:clear-metadata
	@$(PHP_CONT) bin/console doctrine:cache:clear-query
	@$(PHP_CONT) bin/console doctrine:cache:clear-result

entity: ## create or update entity
	$(PHP_CONT) bin/console make:entity
## === 🐛  PHPQA =================================================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run
.PHONY: qa-cs-fixer-dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) php-cs-fixer fix --ansi ./src --rules=@Symfony --verbose
.PHONY: qa-cs-fixer

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) phpstan analyse --ansi ./src --level=3
.PHONY: qa-phpstan

qa-security-checker: ## Run security-checker.
	$(SYMFONY) security:check --ansi --no-interaction
.PHONY: qa-security-checker

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src
.PHONY: qa-phpcpd

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src
.PHONY: qa-php-metrics

qa-lint-yaml: ## Lint yaml files.
	$(SYMFONY_LINT)yaml ./config
.PHONY: qa-lint-yaml

qa-lint-container: ## Lint container.
	$(SYMFONY_LINT)container
.PHONY: qa-lint-container

qa-lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction
.PHONY: qa-lint-schema

qa-audit: ## Run composer audit.
	$(COMPOSER) audit
.PHONY: qa-audit

phpunit: ## Run PHPUnit
	@$(PHP_CONT) ./vendor/bin/phpunit --testdox --colors=always tests
## === ⭐  OTHERS =================================================
before-commit: qa-cs-fixer qa-phpstan   qa-audit test ## Run before commit.
.PHONY: before-commit

make test: ## Run PHPUnit
	@$(PHP_CONT)  php bin/console doctrine:database:create --env=test --if-not-exists
	@$(PHP_CONT) ./vendor/bin/phpunit --testdox --colors=always tests


##—— Doctrine 🏛️ —————————————————————————————————————————————————————————————
dump: ## Dump database
	$(DOCKER) exec -i $(MARIADB) mysqldump -u root -pexample $(DATABASE_ONE) > docker/SQL/$(DATABASE_ONE).sql

sync-database: ## synch database with current entities
	@$(PHP_CONT) bin/console doctrine:migrations:sync-metadata-storage

doctrine: ## Liste toutes les commandes Doctrine disponibles
	@$(PHP_CONT) bin/console doctrine

d-m-d: ## Génère les fichiers de migration pour les différences de schéma détectées
	@$(PHP_CONT) bin/console doctrine:migrations:diff

d-m-m: ## Exécute les migrations non exécutées
	@$(PHP_CONT) bin/console doctrine:migrations:migrate --no-interaction

d-s-v: ## Valide la structure du schéma de la base de données
	@$(PHP_CONT) bin/console doctrine:schema:validate

migration : ## prepare la migration
	@$(PHP_CONT) bin/console make:migration --no-interaction

reset-db:
	@$(PHP_CONT) bin/console doctrine:database:drop --force

create-db :
	@$(PHP_CONT) php bin/console doctrine:database:create

fixture:
	@$(PHP_CONT) bin/console doctrine:fixtures:load  --no-interaction

database: clean-migration reset-db create-db migration d-m-m  fixture

clean-migration: ## Clean xml and executables
	@$(PHP_CONT) sh -c "rm -rf ./migrations/*.php"

factory:
	@$(PHP_CONT) bin/console make:factory

validator: ## Run validator
	@$(PHP_CONT) bin/console make:validator
