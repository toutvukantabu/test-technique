
## Test Technique Wimova


1. Run `make start` pour lancer le projet et créer la database
2. Pour lancer les tests `make test`
3. pour stopper le projet `make down`
4. optional full clean code et tests `make before-commit`


l'accès à api platform se fera à l'adresse  : https://localhost:4443/

l'accès à la documentation : https://localhost:4443/docs?ui=re_doc

### accès admin:

login: admin@localhost  
mdp: admin

Pour les autres comptes même mdp il faudra juste aller récupérer les emails en bdd 

### accès database mariadb:

Pour la connexion en local: 

host: localhost
port : 3307
user: root
mdp: example

nom de la database  : ***database_test_wimova***

nom de la database de test: ***database_test_wimova_test***

****Ps****: j'ai dû apporter une modification mineure à la partie front end car elle ne prenait pas en charge le bearer token , je vous ai mis la version modifiée dans l'email et il y avait aussi une faute à 'password' écrit 'passwortd'
