<?php

namespace App\ApiResource;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class NameDto
{
    public function __construct(
        #[Groups(['user:update', 'user:create', 'user:read'])]
        #[Assert\NotBlank(groups: ['user:create'])]
        #[Assert\Length(
            min: 5, groups: ['user:create', 'user:update']
        )]
        public string $firstname,
        #[Groups(['user:update', 'user:create', 'user:read'])]
        #[Assert\NotBlank(groups: ['user:create'])]
        #[Assert\Length(
            min: 5, groups: ['user:create', 'user:update']
        )]
        public string $lastname
    ) {
    }
}
