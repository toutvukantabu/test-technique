<?php

namespace App\ApiResource;

use ApiPlatform\Doctrine\Orm\State\Options;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Team;
use App\Entity\User;
use App\State\Processor\UserDtoStateProcessor;
use App\State\Provider\UserToDtoProvider;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    shortName: 'user',
    operations: [
        new GetCollection(
            security: 'is_granted("ROLE_USER")'
        ),
        new Post(
            denormalizationContext: ['groups' => ['user:create']],
            validationContext: ['groups' => ['default', 'user:create']]
        ),
        new Patch(
            denormalizationContext: ['groups' => ['user:update']],
            securityPostDenormalize: 'is_granted("ROLE_USER")',
            validationContext: ['groups' => ['user:update']]),
        new Delete(
            validationContext: ['groups' => ['deleteValidation']]),
    ],
    inputFormats: [
        'json' => ['application/json'],
        'xml' => ['application/xml'],
        'jsonld' => ['application/ld+json'],
        ],
    outputFormats: ['json' => ['application/json'], 'xml' => ['application/xml'], 'jsonld' => ['application/ld+json']],
    normalizationContext: ['groups' => ['user:read']],
    denormalizationContext: ['groups' => ['user:create', 'user:update']],
    security: 'is_granted("ROLE_TEAM_LEADER")',
    provider: UserToDtoProvider::class,
    processor: UserDtoStateProcessor::class,
    stateOptions: new Options(User::class),
)]
class UserDto
{
    #[Groups(['user:read'])]
    public ?int $id = null;

    #[Groups(['user:update', 'user:create', 'user:read'])]
    #[Assert\NotBlank(message: 'Please enter email address', groups: ['user:create'])]
    #[Assert\Email(message: 'Please enter valid email address', groups: ['user:create', 'user:update'])]
    public ?string $email = null;

    #[Groups(['user:read'])]
    public ?NameDto $name = null;

    #[Groups(['user:read', 'user:update'])]
    public ?Team $team = null;

    #[ApiProperty(readable: false)]
    public ?string $password = null;

    #[Groups(['user:update', 'user:create'])]
    #[Assert\NotBlank(groups: ['user:create'])]
    #[Assert\Length(
        min: 5, groups: ['user:create', 'user:update']
    )]
    public ?string $firstname = null;

    #[Groups(['user:update', 'user:create'])]
    #[Assert\NotBlank(groups: ['user:create'])]
    #[Assert\Length(
        min: 5, groups: ['user:create', 'user:update']
    )]
    public ?string $lastname = null;
}
