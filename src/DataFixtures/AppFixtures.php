<?php

namespace App\DataFixtures;

use App\Factory\TeamFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        echo 'Fixtures loading...   ';
        userFactory::createOne(
            [
                'roles' => ['ROLE_ADMIN'],
                'email' => 'admin@localhost',
            ]
        );
        echo 'creating 1 admin ok  ';
        $teams = TeamFactory::createMany(10);
        echo 'creating team ok  ';
        foreach ($teams as $team) {
            UserFactory::createOne([
                'team' => $team,
                'roles' => ['ROLE_TEAM_LEADER'],
            ]);
        }
        echo 'creating  team leader ok  ';
        echo 'attempt ...........   ';
        UserFactory::new()->many(100)->create(function () {
            return ['team' => TeamFactory::random()];
        });
        echo 'creating 100 members ok   ';
        echo 'all password are : admin  ';
        echo 'check database to take email connexion   ';
        echo 'done';
        $manager->flush();
    }
}
