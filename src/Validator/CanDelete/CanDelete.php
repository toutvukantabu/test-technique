<?php

namespace App\Validator\CanDelete;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
#[\Attribute()]
class CanDelete extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public string $message = 'The value with id : {{ id }} can not be deleted.';

    public function getTargets(): array|string
    {
        return self::CLASS_CONSTRAINT;
    }
}
