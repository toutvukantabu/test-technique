<?php

namespace App\Validator\CanDelete;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CanDeleteValidator extends ConstraintValidator
{
    public function __construct(private readonly Security $security)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        assert($constraint instanceof CanDelete);
        if (null === $value || '' === $value) {
            return;
        }
        assert($value instanceof User);

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return;
        }
        $user = $this->security->getUser();
        assert($user instanceof User);
        if ($user !== $value && $this->security->isGranted('ROLE_TEAM_LEADER') && $user?->getTeam() === $value->getTeam()) {
            return;
        }
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ id }}', $value->getId())
            ->addViolation();
    }
}
