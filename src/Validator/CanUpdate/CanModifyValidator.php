<?php

namespace App\Validator\CanUpdate;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CanModifyValidator extends ConstraintValidator
{
    public function __construct(private readonly Security $security)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        assert($constraint instanceof CanModify);

        if (null === $value || '' === $value) {
            return;
        }
        $entity = $this->context->getObject();
        $user = $this->security->getUser();
        assert($user instanceof User);

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return;
        }
        if ($entity instanceof User && $entity->getId() === $user->getId()) {
            return;
        }

        if ($entity instanceof User && $this->security->isGranted('ROLE_TEAM_LEADER') && $entity->getTeam() === $user->getTeam()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->addViolation();
    }
}
