<?php

namespace App\State\Provider;

use ApiPlatform\Doctrine\Orm\Paginator;
use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use ApiPlatform\Doctrine\Orm\State\ItemProvider;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\Pagination\TraversablePaginator;
use ApiPlatform\State\ProviderInterface;
use App\ApiResource\NameDto;
use App\ApiResource\UserDto;
use App\Entity\User;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class UserToDtoProvider implements ProviderInterface
{
    public function __construct(
        #[Autowire(service: ItemProvider::class)] private readonly ProviderInterface $provider,
        #[Autowire(service: CollectionProvider::class)] private readonly ProviderInterface $collectionProvider
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
            $users = $this->collectionProvider->provide($operation, $uriVariables, $context);
            assert($users instanceof Paginator);
            $entities = [];
            foreach ($users as $user) {
                $entities[] = $this->mapUserToDto($user);
            }

            return new TraversablePaginator(
                new \ArrayIterator($entities),
                $users->getCurrentPage(),
                $users->getItemsPerPage(),
                $users->getTotalItems()
            );
        }
        $user = $this->provider->provide($operation, $uriVariables, $context);

        return $this->mapUserToDto($user);
    }

    private function mapUserToDto(?User $user): ?UserDto
    {
        if (!$user) {
            return null;
        }
        $dto = new UserDto();
        $dto->id = $user->getId();
        $dto->email = $user->getEmail();
        $dto->name = new NameDto($user->getFirstname(), $user->getLastname());
        $dto->team = $user?->getTeam();

        return $dto;
    }
}
