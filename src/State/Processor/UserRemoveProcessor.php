<?php

namespace App\State\Processor;

use ApiPlatform\Doctrine\Common\State\RemoveProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use ApiPlatform\Validator\ValidatorInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class UserRemoveProcessor implements ProcessorInterface
{
    public function __construct(
        #[Autowire(service : RemoveProcessor::class)] private readonly ProcessorInterface $innerProcessor,
        private readonly ValidatorInterface $validator,
    ) {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        $this->validator->validate($data, ['groups' => ['deleteValidation']]);
        $this->innerProcessor->process($data, $operation, $uriVariables, $context);
    }
}
