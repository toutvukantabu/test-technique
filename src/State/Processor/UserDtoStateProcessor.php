<?php

namespace App\State\Processor;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use ApiPlatform\Validator\ValidatorInterface;
use App\ApiResource\UserDto;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDtoStateProcessor implements ProcessorInterface
{
    public function __construct(
        #[Autowire(service: PersistProcessor::class)] private readonly ProcessorInterface $persistProcessor,
        #[Autowire(service: UserRemoveProcessor::class)] private readonly ProcessorInterface $removeProcessor,
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher, private readonly Security $security, private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @throws \Exception
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        assert($data instanceof UserDto);
        $connectedUser = $this->security->getUser();
        $user = $this->mapDtoToEntity($data);
        assert($user instanceof User);
        if ($operation instanceof DeleteOperationInterface) {
            $this->removeProcessor->process($user, $operation, $uriVariables, $context);

            return null;
        }
        assert($connectedUser instanceof User);
        if (null === $user?->getTeam()) {
            $user?->setTeam($connectedUser?->getTeam());
        }
        $this->validator->validate($user, ['groups' => 'default']);
        $this->persistProcessor->process($user, $operation, $uriVariables, $context);
        $data->id = $user?->getId();

        return $data;
    }

    /**
     * @throws \Exception
     */
    private function mapDtoToEntity(object $dto): object
    {
        assert($dto instanceof UserDto);
        if ($dto->id) {
            $entity = $this->userRepository->find($dto->id);
            if (!$entity) {
                throw new \RuntimeException(sprintf('User %d not found', $dto->id));
            }
        } else {
            $entity = new User();
        }
        /*** @var User $entity */
        if ($dto->email) {
            $entity->setEmail($dto->email);
        }
        if ($dto->password) {
            $entity->setPlainPassword($dto->password);
        }
        if ($dto->firstname) {
            $entity->setFirstname($dto->firstname);
        }
        if ($dto->lastname) {
            $entity->setLastname($dto->lastname);
        }
        if ($dto->password) {
            $entity->setPassword($this->passwordHasher->hashPassword($entity, $dto->password));
        }

        return $entity;
    }
}
